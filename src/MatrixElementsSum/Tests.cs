using Xunit;

namespace MatrixElementSum;

public class Tests
{
	[Fact]
	public void Test1()
	{
		var input = new[]
		{
			new[] { 0, 1, 1, 2 },
			new[] { 0, 5, 0, 0 },
			new[] { 2, 0, 3, 3 }
		};

		var actual = Program.GetMatrixElementsSum(input);

		var expected = 9;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test2()
	{
		var input = new[]
		{
			new[] { 1, 1, 1, 0 },
			new[] { 0, 5, 0, 1 },
			new[] { 2, 1, 3, 10 }
		};

		var actual = Program.GetMatrixElementsSum(input);

		var expected = 9;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test3()
	{
		var input = new[]
		{
			new[] { 1, 1, 1 },
			new[] { 2, 2, 2 },
			new[] { 3, 3, 3 }
		};

		var actual = Program.GetMatrixElementsSum(input);

		var expected = 18;

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void Test4()
	{
		var input = new[]
		{
			new[] { 0 }
		};

		var actual = Program.GetMatrixElementsSum(input);

		var expected = 0;

		Assert.Equal(expected, actual);
	}
}
